var https = require("https");
var path = require("path");
var fs = require("fs");
var pfdtohtml = require("./pdf2html.js");

const express = require('express');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const _ = require('lodash');

const app = express();


// enable files upload
app.use(fileUpload({
    createParentPath: true
}));

//add other middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(morgan('dev'));


//start app 
const hostname = '127.0.0.1';
const port = 3000;
var SourceFile=''
var DestinationFile
app.post('/upload-avatar', async (req, res) => {
    try {
        if(!req.files) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
            let avatar = req.files.avatar;
            console.log("avatar ",avatar)
            //Use the mv() method to place the file in upload directory (i.e. "uploads")
            avatar.mv('./'+avatar.name);
            SourceFile = `./${avatar.name}`;
            console.log('SourceFile',SourceFile)
            
            // pfdtohtml.tohtml(`./${avatar.name}`)
           // send response
            res.send({
                status: true,
                message: 'File is uploaded',
                data: {
                    name: avatar.name,
                    mimetype: avatar.mimetype,
                    size: avatar.size
                }
            });
           
        }
    } catch (err) {
        res.status(500).send(err);
    }
});


app.get('/pdf', function(req, res) {
     pfdtohtml.tohtml(SourceFile);
     console.log('SourceFile',SourceFile)
    res.sendFile('/home/steps/node-pdf/result2.html');
});

app.get('/wajdi',function(req, res) {
    fs.readFile('/home/steps/node-pdf/result2.html',function(err,html){
    res.writeHeader(200, {"Content-Type": "text/html"});  
    res.write(html);  
    res.end(); 
    })
   
    // res.sendFile('/home/steps/node-pdf/result2.html');
})


app.listen(port, hostname,() => 
console.log(`Server running at http://${hostname}:${port}/`)
);